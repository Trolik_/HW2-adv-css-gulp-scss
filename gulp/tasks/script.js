import concat from "gulp-concat";
import minifyjs from "gulp-js-minify";

export const script = () => {
	return app.gulp.src(app.path.src.js)
		.pipe(concat('app.min.js'))
		.pipe(minifyjs())
		.pipe(app.gulp.dest(app.path.build.js))
		.pipe(app.plugins.browsersync.stream());
}
